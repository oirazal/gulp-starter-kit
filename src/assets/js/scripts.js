(function($){
	$(document).ready(function(){
		var a = [
		  "We're up all night 'til the sun",
		  "We're up all night to get some",
		  "We're up all night for good fun",
		  "We're up all night to get lucky"
		];

		// Sans la syntaxe des fonctions fléchées 
		var a2 = a.map(function(s){ return s.length });
		// [31, 30, 31, 31]

		// Avec, on a quelque chose de plus concis
		var a3 = a.map( s => s.length );
	});
}(jQuery));